// **************************************************************
// *
// * This file is part of Puzzle.
// *
// * Puzzle is free software: you can redistribute it and/or
// * modify it under the terms of the GNU Affero General Public
// * License as published by the Free Software Foundation,
// * either version 3 of the License, or (at your option) any
// * later version.
// *
// * Puzzle is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty
// * of MERCHANTABILITY or FINES FOR A PARTICULAR PURPOSE. See
// * the GNU Affero General Public License for more details.
// *
// * You should have received a copy of the GNU Affero General
// * Public License along with Puzzle. If not, see
// * <https://www.gnu.org/licenses/>.
// *
// **************************************************************

import * as React from 'react'
import { renderToString } from 'react-dom/server'
import App from 'components/app'

export default function renderer(): string {
  const html: string = renderToString(<App />)
  return `<!DOCTYPE html>
<html>
  <head>
    <title>Puzzle</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  </head>
  <body>
    <div id="puzzle">${html}</div>
    <script src="/puzzle/js/puzzle.bundle.js"></script>
  </body>
</html>`
}
