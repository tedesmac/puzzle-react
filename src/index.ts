// **************************************************************
// *
// * This file is part of Puzzle.
// *
// * Puzzle is free software: you can redistribute it and/or
// * modify it under the terms of the GNU Affero General Public
// * License as published by the Free Software Foundation,
// * either version 3 of the License, or (at your option) any
// * later version.
// *
// * Puzzle is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty
// * of MERCHANTABILITY or FINES FOR A PARTICULAR PURPOSE. See
// * the GNU Affero General Public License for more details.
// *
// * You should have received a copy of the GNU Affero General
// * Public License along with Puzzle. If not, see
// * <https://www.gnu.org/licenses/>.
// *
// **************************************************************

import * as Hapi from '@hapi/hapi'
import * as Inert from '@hapi/inert'
import * as Path from 'path'
import renderer from 'renderer'

const server: Hapi.Server = new Hapi.Server({
  host: 'localhost',
  port: 8339,
})

server.route([
  {
    method: 'get',
    path: '/',
    handler: function() {
      const html = renderer()
      return html
    },
  },
  {
    method: 'get',
    path: '/puzzle/js/{path*}',
    handler: function(req, res): string {
      const path = Path.resolve('dist/client/', req.params.path)
      return res.file(path)
    },
  },
])

async function start() {
  try {
    await server.start()
  } catch (error) {
    console.error('[Unable to start the server] >>', error)
    process.exit(1)
  }

  await server.register(Inert)

  console.log('Server running at:', server.info.uri)
}

start()
