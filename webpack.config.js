// **************************************************************
// *
// * This file is part of Puzzle.
// *
// * Puzzle is free software: you can redistribute it and/or
// * modify it under the terms of the GNU Affero General Public
// * License as published by the Free Software Foundation,
// * either version 3 of the License, or (at your option) any
// * later version.
// *
// * Puzzle is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty
// * of MERCHANTABILITY or FINES FOR A PARTICULAR PURPOSE. See
// * the GNU Affero General Public License for more details.
// *
// * You should have received a copy of the GNU Affero General
// * Public License along with Puzzle. If not, see
// * <https://www.gnu.org/licenses/>.
// *
// **************************************************************

const path = require('path')

const devtool = 'source-map'

const rules = [
  {
    test: /\.tsx?$/,
    exclude: /node_modules/,
    loader: 'ts-loader',
  },
]

const resolve = {
  extensions: ['.json', '.js', '.jsx', '.ts', '.tsx'],
  alias: {
    client: path.resolve(__dirname, 'src/client'),
    components: path.resolve(__dirname, 'src/components'),
    renderer: path.resolve(__dirname, 'src/renderer'),
  },
}

const mode = 'development'

module.exports = [
  // client
  {
    target: 'web',
    entry: {
      puzzle: path.resolve(__dirname, 'src/client/index.tsx'),
    },
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'dist/client'),
    },
    module: {
      rules,
    },
    devtool,
    mode,
    resolve,
  },
  // server
  {
    target: 'node',
    entry: {
      main: path.resolve(__dirname, 'src/index.ts'),
    },
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, 'dist/server'),
    },
    module: {
      rules,
    },
    devtool,
    mode,
    resolve,
  },
]
